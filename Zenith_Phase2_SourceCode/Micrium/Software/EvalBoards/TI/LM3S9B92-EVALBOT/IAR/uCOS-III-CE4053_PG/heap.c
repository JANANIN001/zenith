#include <os.h>
#include <math.h>
#include <stdio.h>
//#include "splay.h"
#define print_heap 1
extern OS_MEM HeapPartition;
extern CPU_INT08U HeapPartitionStorage[50][100];
extern CPU_INT32U sysceil;
OS_TCB *rdylistptr;

heap* Tree=NULL;
OS_ERR  heap_err;

heap* RR_Rotate_heap(heap* k2)
{
  heap* k1 = k2->lchild;
  k2->lchild = k1->rchild;
  k1->rchild = k2;
  return k1;
}

heap* LL_Rotate_heap(heap* k2)
{
  heap* k1 = k2->rchild;
  k2->rchild = k1->lchild;
  k1->lchild = k2;
  return k1;
}


heap* Heap(OS_TICK ReleaseTime, heap* Tree)
{
  if(!Tree)
    return Tree;
  //return NULL;
  heap head;
  /* head.rchild points to L tree; head.lchild points to R Tree */
  head.lchild = head.rchild = NULL;
  heap* LeftTreeMax_heap = &head;
  heap* RightTreeMin_heap = &head;
  
 
  while(1)
  {
    if(ReleaseTime < Tree->p_tcb_heap->Next_Release)
    {
      if(!Tree->lchild)
        break;
      if(ReleaseTime < Tree->lchild->p_tcb_heap->Next_Release)
      {
        Tree = RR_Rotate_heap(Tree); /* only zig-zig mode need to rotate once,
        because zig-zag mode is handled as zig
        mode, which doesn't require rotate,
        just linking it to R Tree */
        if(!Tree->lchild)
          break;
      }
      /* Link to R Tree */
      RightTreeMin_heap->lchild = Tree;
      RightTreeMin_heap = RightTreeMin_heap->lchild;
      Tree = Tree->lchild;
      RightTreeMin_heap->lchild = NULL;
    }
    else if(ReleaseTime > Tree->p_tcb_heap->Next_Release)
    {
      if(!Tree->rchild)
        break;
      if(ReleaseTime > Tree->rchild->p_tcb_heap->Next_Release)
      {
        Tree = LL_Rotate_heap(Tree);
        if(!Tree->rchild)
          break;
      }
      /* Link to L Tree */
      LeftTreeMax_heap->rchild = Tree;
      LeftTreeMax_heap = LeftTreeMax_heap->rchild;
      Tree = Tree->rchild;
      LeftTreeMax_heap->rchild = NULL;
    }
    else
      break;
  }
  /* assemble L Tree, Middle Tree and R tree together */
  LeftTreeMax_heap->rchild = Tree->lchild;
  RightTreeMin_heap->lchild = Tree->rchild;
  Tree->lchild = head.rchild;
  Tree->rchild = head.lchild;
  
  return Tree;
}

heap* New_Node_heap(OS_TCB *p_tcb_)
{
 
  heap *p_node = (heap *)OSMemGet((OS_MEM *)&HeapPartition,(OS_ERR *)&heap_err);


  p_node->p_tcb_heap = p_tcb_;
  p_node->lchild = p_node->rchild = NULL;
  return p_node;
}


heap* Insert_heap(OS_TCB *p_tcb_, heap* Tree)
{
 
  heap* p_node = NULL;
  
  
  p_node = (heap *)OSMemGet((OS_MEM *)&HeapPartition,(OS_ERR *)&heap_err);
  
#ifdef print_heap
  printf("\nNew Memory Allocated");
#endif
  p_node->p_tcb_heap = p_tcb_;
  p_node->lchild = p_node->rchild = NULL;
  
  /***************initialize the stack*************/
  p_node->p_tcb_heap->StkPtr = OSTaskStkInit(p_node->p_tcb_heap->TaskEntryAddr,
                                             p_node->p_tcb_heap->TaskEntryArg,
                                             p_node->p_tcb_heap->StkBasePtr,
                                             p_node->p_tcb_heap->StkLimitPtr,
                                             p_node->p_tcb_heap->StkSize,
                                             p_node->p_tcb_heap->Opt);
  
  
  if(!Tree)
  {
#ifdef print_heap
    printf("\nFirst Tree...");
#endif
    Tree = p_node;
    p_node = NULL;
    return Tree;
  }
  Tree = Heap(p_tcb_->Next_Release, Tree);
  if(p_tcb_->Next_Release == Tree->p_tcb_heap->Next_Release)   //duplicate values
  {
    if(Tree->dup_nbr == 0)
    {
      Tree->next = p_node;
      Tree->dup_nbr++;
      p_node = NULL;
      return Tree;
    }
    else
    {
      heap *temp = Tree;
      (temp->dup_nbr)++;
      while(temp->next != NULL)
      {
        temp = temp->next;
      }
      temp->next = p_node;
      p_node = NULL;
      return Tree;
    }
  }
  /* This is BST that, all keys <= Tree->key is in Tree->lchild, all keys >
  Tree->key is in Tree->rchild. (This BST doesn't allow duplicate keys.) NOW IT DOES...!!! */
  if(p_tcb_->Next_Release < Tree->p_tcb_heap->Next_Release)
  {
    p_node->lchild = Tree->lchild;
    p_node->rchild = Tree;
    Tree->lchild = NULL;
    Tree = p_node;

  }
  else if(p_tcb_->Next_Release > Tree->p_tcb_heap->Next_Release)
  {
    p_node->rchild = Tree->rchild;
    p_node->lchild = Tree;
    Tree->rchild = NULL;
    Tree = p_node;

  }
  else
    return Tree;
  p_node = NULL;
  return Tree;
}

/****************************************DELETE TASK**********************************************/
heap* Delete_heap(OS_TCB *p_tcb_, heap* Tree)
{
  heap* temp;
  heap* prev_temp;
  if(!Tree)
    return Tree;
  //return NULL;
  Tree = Heap(p_tcb_->Next_Release, Tree);
  if(p_tcb_->Next_Release != Tree->p_tcb_heap->Next_Release) // No such node in heap tree
    return Tree;
  // Node with this Next_Release value found, now have to search for a particular TCB
  else
  {
    // node to be deleted is the only node
    if((Tree->p_tcb_heap == p_tcb_) &&(Tree->dup_nbr == 0))
    {
      if(!Tree->lchild)
      {
        temp = Tree;
        Tree = Tree->rchild;
      }
      else
      {
        temp = Tree;
        
        Tree = Heap(p_tcb_->Next_Release, Tree->lchild);
        Tree->rchild = temp->rchild;
      }
      OSMemPut((OS_MEM *)&HeapPartition,(heap *)temp,(OS_ERR *)&heap_err);
      //free(temp);
      return Tree;
    }
    
    // else if node to be deleted is the first node, but duplicates are present
    else if((Tree->p_tcb_heap == p_tcb_) &&(Tree->dup_nbr > 0))
    {
      temp = Tree->next;
      Tree->dup_nbr = Tree->dup_nbr - 1;
      Tree->next = temp->next;
      Tree->p_tcb_heap = temp->p_tcb_heap;
      OSMemPut((OS_MEM *)&HeapPartition,(heap *)temp,(OS_ERR *)&heap_err);
      return Tree;
    }
    
    //else if node to be deleted in inside linked list, have to traverse till there
    //and then delete
    else if((Tree->dup_nbr > 0) && (Tree->p_tcb_heap != p_tcb_))
    {
      temp = Tree;
      while(temp->p_tcb_heap != p_tcb_)
      {
        prev_temp = temp;
        temp = temp->next;
      }
      Tree->dup_nbr = Tree->dup_nbr - 1;
      prev_temp->next = temp->next;
      OSMemPut((OS_MEM *)&HeapPartition,(heap *)temp,(OS_ERR *)&heap_err);
      return Tree;
    }
  }
}

void ReadyListInsert(void)
{
  heap *MinNode = find_min();
  
  if(MinNode == NULL)
  {
  }
  else if(rdylistptr == NULL)
  {
    if((MinNode != NULL) && (MinNode->p_tcb_heap->Prio < sysceil))
    {
      OS_TaskRdy(MinNode->p_tcb_heap);
      rdylistptr = MinNode->p_tcb_heap;
      OSTaskQty++;
      Tree = Delete_heap(MinNode->p_tcb_heap, Tree);
    }
    else if((MinNode != NULL) && (MinNode->p_tcb_heap->Prio >= sysceil))
    {
    // Insert into Blocked Task data Structure
      blockedroot = Insert_blkd(MinNode->p_tcb_heap, blockedroot);
      Tree = Delete_heap(MinNode->p_tcb_heap, Tree);
    }
  }
  
  else if((MinNode->p_tcb_heap-> Next_Release < rdylistptr -> Next_Release) && (MinNode->p_tcb_heap->Prio < sysceil))
  {    
    CPU_SR_ALLOC();
    OS_CRITICAL_ENTER();
    OS_RdyListRemove(rdylistptr);
             
    OS_CRITICAL_EXIT_NO_SCHED();
    Tree = Insert_heap(rdylistptr, Tree);
    OS_TaskRdy(MinNode->p_tcb_heap); 
    rdylistptr = MinNode->p_tcb_heap;

    Tree = Delete_heap(MinNode->p_tcb_heap, Tree);
  }
  
   else if((MinNode->p_tcb_heap-> Next_Release < rdylistptr -> Next_Release) && (MinNode->p_tcb_heap->Prio > sysceil))
  {    
   
  }
  
  else if((MinNode->p_tcb_heap-> Next_Release > rdylistptr -> Next_Release) && (MinNode->p_tcb_heap->Prio > sysceil))
  {    
   
  }
}

// called from OSRecTaskDelete function
void rdylist_delete(void)
{
  heap* MinNode = find_min();
  if(MinNode == NULL)
  {
    rdylistptr = NULL;
  }
  else 
  {
    OS_TaskRdy(MinNode->p_tcb_heap); 
    rdylistptr = MinNode->p_tcb_heap;
    OSTaskQty++;
    Tree = Delete_heap(MinNode->p_tcb_heap, Tree);
  }
}



heap* Search_heap(OS_TICK key, heap* Tree)
{
  return Heap(key, Tree);
}


heap* find_min()
{
  heap* current = Tree;
  if(current == NULL)
  {
    return NULL;
  }
  else while(current->lchild != NULL)
  {
    current = current->lchild;
  }
  return current;
}

void blocked_handler()
{
splayblkd* minNode;
minNode = find_min_blkd();
  if((rdylistptr == NULL) && (minNode != NULL) && (minNode->p_tcb_splayblkd->Prio < sysceil))
  {
  OS_TaskRdy(minNode->p_tcb_splayblkd);
  rdylistptr = minNode->p_tcb_splayblkd;
  OSTaskQty++;
   
  blockedroot = Delete_blkd(minNode->p_tcb_splayblkd, blockedroot);
  }
  else if((minNode->p_tcb_splayblkd-> Next_Release < rdylistptr -> Next_Release) && (minNode->p_tcb_splayblkd->Prio < sysceil))
  {    
    CPU_SR_ALLOC();
    OS_CRITICAL_ENTER();
    OS_RdyListRemove(rdylistptr);
           
    OS_CRITICAL_EXIT_NO_SCHED();
    Tree = Insert_heap(rdylistptr, Tree);
    OS_TaskRdy(minNode->p_tcb_splayblkd); 
    rdylistptr = minNode->p_tcb_splayblkd;
   
    blockedroot = Delete_blkd(minNode->p_tcb_splayblkd, blockedroot);
  }
  else{
        Tree = Insert_heap(minNode->p_tcb_splayblkd, Tree);
  }
}