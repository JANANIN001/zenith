#include <os.h>
#include <math.h>
#include <stdio.h>
#include "splay.h"
#define print_avl 1
extern OS_MEM AVLPartition;
extern CPU_INT08U AVLPartitionStorage[50][100];

avl* root=NULL;
OS_ERR  err;   

avl* RR_Rotate(avl* k2)
{
	avl* k1 = k2->lchild;
	k2->lchild = k1->rchild;
	k1->rchild = k2;
	return k1;
}

avl* LL_Rotate(avl* k2)
{
	avl* k1 = k2->rchild;
	k2->rchild = k1->lchild;
	k1->lchild = k2;
	return k1;
}


avl* Avl(OS_TICK next_release, avl* root)
{
	if(!root)
                  return root;
		//return NULL;
	avl header;
	/* header.rchild points to L tree; header.lchild points to R Tree */
	header.lchild = header.rchild = NULL;
	avl* LeftTreeMax = &header;
	avl* RightTreeMin = &header;

	
	while(1)
	{
		if(next_release < root->p_tcb_avl->Next_Release)
		{
			if(!root->lchild)
				break;
			if(next_release < root->lchild->p_tcb_avl->Next_Release)
			{
				root = RR_Rotate(root); 				if(!root->lchild)
					break;
			}
			/* Link to R Tree */
			RightTreeMin->lchild = root;
			RightTreeMin = RightTreeMin->lchild;
			root = root->lchild;
			RightTreeMin->lchild = NULL;
		}
		else if(next_release > root->p_tcb_avl->Next_Release)
		{
			if(!root->rchild)
				break;
			if(next_release > root->rchild->p_tcb_avl->Next_Release)
			{
				root = LL_Rotate(root);
				if(!root->rchild)
					break;
			}
			/* Link to L Tree */
			LeftTreeMax->rchild = root;
			LeftTreeMax = LeftTreeMax->rchild;
			root = root->rchild;
			LeftTreeMax->rchild = NULL;
		}
		else
			break;
	}
	/* assemble L Tree, Middle Tree and R tree together */
	LeftTreeMax->rchild = root->lchild;
	RightTreeMin->lchild = root->rchild;
	root->lchild = header.rchild;
	root->rchild = header.lchild;

	return root;
}

avl* New_Node(OS_TCB *p_tcb_)
{
  
	//avl* p_node = new avl;
        avl *p_node = (avl *)OSMemGet((OS_MEM *)&AVLPartition,(OS_ERR *)&err);
	//avl *p_node = (avl *)malloc(sizeof(avl));
#ifdef print_splay
        printf("\nNew Memory Allocated");
#endif
	p_node->p_tcb_avl = p_tcb_;
	p_node->lchild = p_node->rchild = NULL;
        return p_node;
}


avl* Insert(OS_TCB *p_tcb_, avl* root)
{
  	//static avl* p_node = NULL;
	avl* p_node = NULL;
               
        p_node = (avl *)OSMemGet((OS_MEM *)&AVLPartition,(OS_ERR *)&err);
        
#ifdef print_avl
        printf("\nNew Memory Allocated");
#endif
	p_node->p_tcb_avl = p_tcb_;
	p_node->lchild = p_node->rchild = NULL;
        
        /***************initialize the stack*************/
        p_node->p_tcb_avl->StkPtr = OSTaskStkInit(p_node->p_tcb_avl->TaskEntryAddr, p_node->p_tcb_avl->TaskEntryArg, p_node->p_tcb_avl->StkBasePtr,
                                p_node->p_tcb_avl->StkLimitPtr, p_node->p_tcb_avl->StkSize, p_node->p_tcb_avl->Opt);
        
        
        if(!root)
	{
#ifdef print_avl
                printf("\nFirst root...");
#endif
		root = p_node;
		p_node = NULL;
		return root;
	}
	root = Avl(p_tcb_->Next_Release, root);
	if(p_tcb_->Next_Release == root->p_tcb_avl->Next_Release)   //duplicate values
    {
        if(root->duplicate_nbr == 0)
        {
            root->next = p_node;
            root->duplicate_nbr++;
            p_node = NULL;
            return root;
        }
        else
        {
            avl *temp = root;
            (temp->duplicate_nbr)++;
            while(temp->next != NULL)
            {
                temp = temp->next;
            }
            temp->next = p_node;
            p_node = NULL;
            return root;
        }
    }
	/* This is BST that, all keys <= root->key is in root->lchild, all keys >
	   root->key is in root->rchild. (This BST doesn't allow duplicate keys.) NOW IT DOES...!!! */
	if(p_tcb_->Next_Release < root->p_tcb_avl->Next_Release)
	{
		p_node->lchild = root->lchild;
		p_node->rchild = root;
		root->lchild = NULL;
		root = p_node;
                #ifdef print_avl
                printf("\n root left 1...");
#endif
	}
	else if(p_tcb_->Next_Release > root->p_tcb_avl->Next_Release)
	{
		p_node->rchild = root->rchild;
		p_node->lchild = root;
		root->rchild = NULL;
		root = p_node;
                #ifdef print_avl
                printf("\n root right 1...");
#endif
	}
	else
		return root;
	p_node = NULL;
	return root;
}

/****************************************DELETE TASK**********************************************/
avl* Delete(OS_TCB *p_tcb_, avl* root)
{
	avl* temp;
        avl* prev_temp;
	if(!root)
                  return root;
		//return NULL;
	root = Avl(p_tcb_->Next_Release, root);
	if(p_tcb_->Next_Release != root->p_tcb_avl->Next_Release) // No such node in avl tree
		return root;
	// Node with this Next_Release value found, now have to search for a particular TCB
        else
	{
                // node to be deleted is the only node
                if((root->p_tcb_avl == p_tcb_) &&(root->duplicate_nbr == 0))
                {
                  if(!root->lchild)
                  {
                          temp = root;
                          root = root->rchild;
                  }
                  else
                  {
                          temp = root;
                          /*Note: Since key == root->key, so after Avl(key, root->lchild),
                            the tree we get will have no right child tree. (key > any key in
			  oot->lchild)*/
                          root = Avl(p_tcb_->Next_Release, root->lchild);
                          root->rchild = temp->rchild;
                  }
                  OSMemPut((OS_MEM *)&AVLPartition,(avl *)temp,(OS_ERR *)&err);
                  //free(temp);
                  return root;
                 }
                
                // else if node to be deleted is the first node, but duplicates are present
                else if((root->p_tcb_avl == p_tcb_) &&(root->duplicate_nbr > 0))
                {
                  temp = root->next;
                  root->duplicate_nbr = root->duplicate_nbr - 1;
                  root->next = temp->next;
                  root->p_tcb_avl = temp->p_tcb_avl;
                  OSMemPut((OS_MEM *)&AVLPartition,(avl *)temp,(OS_ERR *)&err);
                  return root;
                }
                
                //else if node to be deleted in inside linked list, have to traverse till there
                //and then delete
                else if((root->duplicate_nbr > 0) && (root->p_tcb_avl != p_tcb_))
                {
                  temp = root;
                  while(temp->p_tcb_avl != p_tcb_)
                  {
                    prev_temp = temp;
                    temp = temp->next;
                  }
                  root->duplicate_nbr = root->duplicate_nbr - 1;
                  prev_temp->next = temp->next;
                  OSMemPut((OS_MEM *)&AVLPartition,(avl *)temp,(OS_ERR *)&err);
                  return root;
                }
        }
}

avl* Search(OS_TICK key, avl* root)
{
	return Avl(key, root);
}

