#include <os.h>
#include <math.h>
#include <stdio.h>
//#include "splay.h"
#define print_splayblkd 1
extern OS_MEM blockedPartition;
extern CPU_INT08U blockedPartitionStorage[50][100];

splayblkd* blockedroot=NULL;
OS_ERR  block_err;   


splayblkd* RR_Rotate_blkd(splayblkd* k2)
{
	splayblkd* k1 = k2->lchild;
	k2->lchild = k1->rchild;
	k1->rchild = k2;
	return k1;
}


splayblkd* LL_Rotate_blkd(splayblkd* k2)
{
	splayblkd* k1 = k2->rchild;
	k2->rchild = k1->lchild;
	k1->lchild = k2;
	return k1;
}

splayblkd* Splay_blkd(OS_TICK next_release, splayblkd* root)
{
	if(!root)
                  return root;
		//return NULL;
	splayblkd header;
	/* header.rchild points to L tree; header.lchild points to R Tree */
	header.lchild = header.rchild = NULL;
	splayblkd* LeftTreeMax = &header;
	splayblkd* RightTreeMin = &header;

	
	while(1)
	{
		if(next_release < root->p_tcb_splayblkd->Next_Release)
		{
			if(!root->lchild)
				break;
			if(next_release < root->lchild->p_tcb_splayblkd->Next_Release)
			{
				root = RR_Rotate_blkd(root); /* only zig-zig mode need to rotate once,
										   because zig-zag mode is handled as zig
										   mode, which doesn't require rotate,
										   just linking it to R Tree */
				if(!root->lchild)
					break;
			}
			/* Link to R Tree */
			RightTreeMin->lchild = root;
			RightTreeMin = RightTreeMin->lchild;
			root = root->lchild;
			RightTreeMin->lchild = NULL;
		}
		else if(next_release > root->p_tcb_splayblkd->Next_Release)
		{
			if(!root->rchild)
				break;
			if(next_release > root->rchild->p_tcb_splayblkd->Next_Release)
			{
				root = LL_Rotate_blkd(root);/* only zag-zag mode need to rotate once,
										  because zag-zig mode is handled as zag
										  mode, which doesn't require rotate,
										  just linking it to L Tree */
				if(!root->rchild)
					break;
			}
			/* Link to L Tree */
			LeftTreeMax->rchild = root;
			LeftTreeMax = LeftTreeMax->rchild;
			root = root->rchild;
			LeftTreeMax->rchild = NULL;
		}
		else
			break;
	}
	/* assemble L Tree, Middle Tree and R tree together */
	LeftTreeMax->rchild = root->lchild;
	RightTreeMin->lchild = root->rchild;
	root->lchild = header.rchild;
	root->rchild = header.lchild;

	return root;
}

splayblkd* New_Node_blkd(OS_TCB *p_tcb_)
{
  
	//splayblkd* p_node = new splayblkd;
        splayblkd *p_node = (splayblkd *)OSMemGet((OS_MEM *)&blockedPartition,(OS_ERR *)&block_err);
	//splayblkd *p_node = (splayblkd *)malloc(sizeof(splayblkd));
#ifdef print_splayblkd
        printf("\nNew Memory Allocated");
#endif
	p_node->p_tcb_splayblkd = p_tcb_;
	p_node->lchild = p_node->rchild = NULL;
        return p_node;
}


splayblkd* Insert_blkd(OS_TCB *p_tcb_, splayblkd* root)
{
  	
	splayblkd* p_node = NULL;
               
        p_node = (splayblkd *)OSMemGet((OS_MEM *)&blockedPartition,(OS_ERR *)&block_err);
        
#ifdef print_splayblkd
        printf("\nNew Memory Allocated");
#endif
	p_node->p_tcb_splayblkd = p_tcb_;
	p_node->lchild = p_node->rchild = NULL;
        
        /***************initialize the stack*************/
        p_node->p_tcb_splayblkd->StkPtr = OSTaskStkInit(p_node->p_tcb_splayblkd->TaskEntryAddr, p_node->p_tcb_splayblkd->TaskEntryArg, p_node->p_tcb_splayblkd->StkBasePtr,
                                p_node->p_tcb_splayblkd->StkLimitPtr, p_node->p_tcb_splayblkd->StkSize, p_node->p_tcb_splayblkd->Opt);
        
        
        if(!root)
	{
#ifdef print_splayblkd
                printf("\nFirst root...");
#endif
		root = p_node;
		p_node = NULL;
		return root;
	}
	root = Splay_blkd(p_tcb_->Next_Release, root);
	if(p_tcb_->Next_Release == root->p_tcb_splayblkd->Next_Release)   //duplicate values
    {
        if(root->duplicate_nbr == 0)
        {
            root->next = p_node;
            root->duplicate_nbr++;
            p_node = NULL;
            return root;
        }
        else
        {
            splayblkd *temp = root;
            (temp->duplicate_nbr)++;
            while(temp->next != NULL)
            {
                temp = temp->next;
            }
            temp->next = p_node;
            p_node = NULL;
            return root;
        }
    }
	/* This is BST that, all keys <= root->key is in root->lchild, all keys >
	   root->key is in root->rchild. (This BST doesn't allow duplicate keys.) NOW IT DOES...!!! */
	if(p_tcb_->Next_Release < root->p_tcb_splayblkd->Next_Release)
	{
		p_node->lchild = root->lchild;
		p_node->rchild = root;
		root->lchild = NULL;
		root = p_node;
               
	}
	else if(p_tcb_->Next_Release > root->p_tcb_splayblkd->Next_Release)
	{
		p_node->rchild = root->rchild;
		p_node->lchild = root;
		root->rchild = NULL;
		root = p_node;
                
	}
	else
		return root;
	p_node = NULL;
	return root;
}

/****************************************DELETE TASK**********************************************/
splayblkd* Delete_blkd(OS_TCB *p_tcb_, splayblkd* root)
{
	splayblkd* temp;
        splayblkd* prev_temp;
	if(!root)
                  return root;
		
	root = Splay_blkd(p_tcb_->Next_Release, root);
	if(p_tcb_->Next_Release != root->p_tcb_splayblkd->Next_Release) // No such node in splayblkd tree
		return root;
	// Node with this Next_Release value found, now have to search for a particular TCB
        else
	{
                // node to be deleted is the only node
                if((root->p_tcb_splayblkd == p_tcb_) &&(root->duplicate_nbr == 0))
                {
                  if(!root->lchild)
                  {
                          temp = root;
                          root = root->rchild;
                  }
                  else
                  {
                          temp = root;
                          /*Note: Since key == root->key, so after splayblkd(key, root->lchild),
                            the tree we get will have no right child tree. (key > any key in
			  oot->lchild)*/
                          root = Splay_blkd(p_tcb_->Next_Release, root->lchild);
                          root->rchild = temp->rchild;
                  }
                  OSMemPut((OS_MEM *)&blockedPartition,(splayblkd *)temp,(OS_ERR *)&block_err);
                  
                  return root;
                 }
                
                // else if node to be deleted is the first node, but duplicates are present
                else if((root->p_tcb_splayblkd == p_tcb_) &&(root->duplicate_nbr > 0))
                {
                  temp = root->next;
                  root->duplicate_nbr = root->duplicate_nbr - 1;
                  root->next = temp->next;
                  root->p_tcb_splayblkd = temp->p_tcb_splayblkd;
                  OSMemPut((OS_MEM *)&blockedPartition,(splayblkd *)temp,(OS_ERR *)&block_err);
                  return root;
                }
                
                //else if node to be deleted in inside linked list, have to traverse till there
                //and then delete
                else if((root->duplicate_nbr > 0) && (root->p_tcb_splayblkd != p_tcb_))
                {
                  temp = root;
                  while(temp->p_tcb_splayblkd != p_tcb_)
                  {
                    prev_temp = temp;
                    temp = temp->next;
                  }
                  root->duplicate_nbr = root->duplicate_nbr - 1;
                  prev_temp->next = temp->next;
                  OSMemPut((OS_MEM *)&blockedPartition,(splayblkd *)temp,(OS_ERR *)&block_err);
                  return root;
                }
        }
}

splayblkd* Search_blkd(OS_TICK key, splayblkd* root)
{
	return Splay_blkd(key, root);
}

splayblkd* find_min_blkd()
{
  splayblkd* current = blockedroot;
  if(current == NULL)
  {
    return NULL;
  }
  else while(current->lchild != NULL)
  {
    current = current->lchild;
  }
  return current;
}

