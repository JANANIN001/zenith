#include <stdlib.h>
#include <stdio.h>
#include <os.h>
//#include "os_mutex.c"

int size;  /* number of nodes in the mutexTree */
/* Not actually needed for any of the operations */

OS_MEM MemBlock;
OS_ERR err_mutex;
CPU_INT32U Memory_alloc_init[50][100];

OS_TICK sysceil = 100000;

typedef struct tree_node mutexTree;

struct tree_node {
    mutexTree * left, * right;
    OS_MUTEX *p_mut;
    CPU_INT32U ctr;
};

mutexTree *root_mutex = NULL;

void splay_create();
mutexTree* minValue(mutexTree * node);
mutexTree * delete_mutex(OS_MUTEX *p_mt, mutexTree * t);
mutexTree * splay_mutex (CPU_INT32U rceil, mutexTree * t);
mutexTree * insert_mutex(mutexTree * t, OS_MUTEX * p_mut);

void splay_create(){
    OSMemCreate((OS_MEM*) &MemBlock, "Memory_alloc_init", &Memory_alloc_init[0][0], (OS_MEM_QTY)10, (OS_MEM_SIZE)(20*sizeof(CPU_INT32U)), &err_mutex);
}

mutexTree* minValue(mutexTree * node)
{
    
    mutexTree* current = node;
    
    /* loop down to find the leftmost leaf */
    while (current->left != NULL) {
        current = current->left;
    }
    return (current);
}

// Most frequently accessed node to the top as root
    
mutexTree * splay_mutex (CPU_INT32U rceil, mutexTree * t) {
    /* Simple top down splay, not requiring i to be in the mutexTree t.  */
    /* What it does is described above.                             */
    mutexTree N, *l, *r, *y;
    if (t == NULL) return t;
    N.left = N.right = NULL;
    N.ctr = 0;
    l = r = &N;
    
    for (;;) {
        if (rceil < t->p_mut->R_Ceil) {
            if (t->left == NULL) break;
            if (rceil < t->left->p_mut->R_Ceil) {
                y = t->left;                           /* rotate right */
                t->left = y->right;
                y->right = t;
                t = y;
                if (t->left == NULL) break;
            }
            r->left = t;                               /* link right */
            r = t;
            t = t->left;
        } else if (rceil > t->p_mut->R_Ceil) {
            if (t->right == NULL) break;
            if (rceil > t->right->p_mut->R_Ceil) {
                y = t->right;                          /* rotate left */
                t->right = y->left;
                y->left = t;
                t = y;
                if (t->right == NULL) break;
            }
            l->right = t;                              /* link left */
            l = t;
            t = t->right;
        } else {
            break;
        }
    }
    l->right = t->left;                                /* assemble */
    r->left = t->right;
    t->left = N.right;
    t->right = N.left;
    return t;
}

//Insert Mutex to the Data Structure

mutexTree * insert_mutex(mutexTree * t, OS_MUTEX *p_mt) {
    /* Insert i into the mutexTree t, unless it's already there.    */
    /* Return a pointer to the resulting mutexTree.                 */
    mutexTree * new;
    
    new = (mutexTree *)OSMemGet(&MemBlock,&err_mutex);;
    if (new == NULL) {
        printf("Ran out of space\n");
        exit(1);
    }
    new->p_mut=p_mt;
    if (t == NULL) {
        size = 1;
        t = new;
        root_mutex = t;
        return root_mutex;
    }
    t = splay_mutex(p_mt->R_Ceil,t);
    if (p_mt->R_Ceil < t->p_mut->R_Ceil) {
        new->left = t->left;
        new->right = t;
        t->left = NULL;
        t = new;
        size ++;
        //return t;
    } else if (p_mt->R_Ceil > t->p_mut->R_Ceil) {
        new->right = t->right;
        new->left = t;
        t->right = NULL;
        size++;
        t = new;
        //return t;
    } else { /* We get here if it's already in the mutexTree */
        /* Don't add it again                      */
        //free(new);
        if(t->ctr > 0)
        {
        OSMemPut((OS_MEM *)&MemBlock,(mutexTree *)new,(OS_ERR *)&err_mutex);
        }
        t->ctr = t->ctr+1;
        root_mutex = t;
        return root_mutex;
    }
    new = NULL;
    root_mutex = t;
    return root_mutex;
}

//Delete Mutex from Data Structure

mutexTree * delete_mutex(OS_MUTEX *p_mt, mutexTree * t) {
    /* Deletes i from the mutexTree if it's there.               */
    /* Return a pointer to the resulting mutexTree.              */
    mutexTree * x;
    if (t==NULL) return NULL;
    t = splay_mutex(p_mt->R_Ceil,t);
    if (p_mt->R_Ceil == t->p_mut->R_Ceil) {               /* found it */
        if (t->left == NULL) {
            x = t;
            t = t->right;
        } else {
            x = t;
            t = splay_mutex(p_mt->R_Ceil, t->left);
            t->right = x->right;
        }
        size--;
        //free(t);
        if(t->ctr == 0){
        OSMemPut((OS_MEM *)&MemBlock,(mutexTree *)x,(OS_ERR *)&err_mutex);
        }
        t->ctr = t->ctr-1;
        root_mutex = t;
        return root_mutex;
    }
    root_mutex = t;
    return root_mutex;                         /* It wasn't there */
}

// Updation of System ceiling based on task resource ceiling

void update_sysceil()
{
  mutexTree *tempnode;
  //CPU_INT32U tempceil;
  tempnode = minValue(root_mutex);
  sysceil = tempnode->p_mut->R_Ceil;
}


