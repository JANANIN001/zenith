/*#include<stdio.h>
#include<os.h>
#inlcude<stdlib.h>

OS_MEM MemBlock;
CPU_INT32U Memory_alloc_init[10][20];

typedef struct taskparams
{
    OS_TCB *p_tcb;
    TASK_PERIOD period;
    RELEASE_TIME ReleaseTime;
       
}TaskParams;

struct AVLTreeNode
{
    struct node *left,*right;
    int ht;
    TaskParams node;
    
}


node *insert(node *,int);
node *Delete(node *,int);
node *find_min(node *);
int height( node *);
node *rotateright(node *);
node *rotateleft(node *);
node *RR(node *);
node *LL(node *);
node *LR(node *);
node *RL(node *);
int BF(node *);

//node * AvlTasksert

node * insert(node *T,int x,OS_TCB *tcb)
{
  OS_ERR err;
  OSMemCreate((OS_MEM*) &MemBlock, "Memory_alloc_init", &Memory_alloc_init[0][0], (OS_MEM_QTY)10, (OS_MEM_SIZE)(10*sizeof(CPU_INT32U)), &err);
    if(T==NULL)
    {
        T=(node*)OSMemGet(&MemBlock,&err);
        T->tp=x;
        T->tcb=tcb;
        T->left=NULL;
        T->right=NULL;
    }
    else
        if(x > T->tp)        // insert in right subtree
        {
            
          T->right=insert(T->right,x,tcb);
            if(BF(T)==-2)
                if(x>T->right->tp)
                    T=RR(T);
                else
                    T=RL(T);
        }
        else
            if(x<T->tp)
            {
                T->left=insert(T->left,x,tcb);
                if(BF(T)==2)
                    if(x < T->left->tp)
                        T=LL(T);
                    else
                        T=LR(T);
            }

        T->ht=height(T);

        return(T);
}

node * Delete(node *T,int x)
{
    node *p;

    if(T==NULL)
    {
        return NULL;
    }
    else
        if(x > T->tp)        // insert in right subtree
        {
            T->right=Delete(T->right,x);
            if(BF(T)==2)
                if(BF(T->left)>=0)
                    T=LL(T);
                else
                    T=LR(T);
        }
        else
            if(x<T->tp)
            {
                T->left=Delete(T->left,x);
                if(BF(T)==-2)    //Rebalance during windup
                    if(BF(T->right)<=0)
                        T=RR(T);
                    else
                        T=RL(T);
            }
            else
            {
                //tp to be deleted is found
                if(T->right!=NULL)
                {    //delete its inorder succesor
                    p=T->right;

                    while(p->left!= NULL)
                        p=p->left;

                    T->tp=p->tp;
                    T->right=Delete(T->right,p->tp);

                    if(BF(T)==2)//Rebalance during windup
                        if(BF(T->left)>=0)
                            T=LL(T);
                        else
                            T=LR(T);\
                }
                else
                    return(T->left);
            }
    T->ht=height(T);
    return(T);
}

node * find_min(node *T)
{
  //node *min_node;
  if(T==NULL)
  {
    printf("Tree is empty");
  }
  while((T!=NULL) && (T->left!=NULL))
  {
    T = T->left;
  }
  printf("Minimum node %d", T->tp);
  return (T);

}

int height(node *T)
{
    int lh,rh;
    if(T==NULL)
        return(0);

    if(T->left==NULL)
        lh=0;
    else
        lh=1+T->left->ht;

    if(T->right==NULL)
        rh=0;
    else
        rh=1+T->right->ht;

    if(lh>rh)
        return(lh);

    return(rh);
}

node * rotateright(node *x)
{
    node *y;
    y=x->left;
    x->left=y->right;
    y->right=x;
    x->ht=height(x);
    y->ht=height(y);
    return(y);
}

node * rotateleft(node *x)
{
    node *y;
    y=x->right;
    x->right=y->left;
    y->left=x;
    x->ht=height(x);
    y->ht=height(y);

    return(y);
}

node * RR(node *T)
{
    T=rotateleft(T);
    return(T);
}

node * LL(node *T)
{
    T=rotateright(T);
    return(T);
}

node * LR(node *T)
{
    T->left=rotateleft(T->left);
    T=rotateright(T);

    return(T);
}

node * RL(node *T)
{
    T->right=rotateright(T->right);
    T=rotateleft(T);
    return(T);
}

int BF(node *T)
{
    int lh,rh;
    if(T==NULL)
        return(0);

    if(T->left==NULL)
        lh=0;
    else
        lh=1+T->left->ht;

    if(T->right==NULL)
        rh=0;
    else
        rh=1+T->right->ht;

    return(lh-rh);
}
*/