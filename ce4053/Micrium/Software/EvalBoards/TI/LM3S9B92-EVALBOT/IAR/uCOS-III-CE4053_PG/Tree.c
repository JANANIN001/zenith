#define _XOPEN_SOURCE 500 /* Enable certain library functions (strdup) on linux. See feature_test_macros(7) */

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <assert.h>

OS_MEM MemBlock;
OS_ERR err;
CPU_INT32U Memory_alloc_init[10][20];

typedef struct taskparams TaskParams;
typedef struct avl_node_s avl_node_t;
typedef struct avl_tree_s avl_tree_t;
CPU_BOOLEAN isFirst = 1;
avl_node_t* tree_1;
avl_tree_t *tree;
avl_tree_t *avl_create();
avl_node_t *avl_create_node(TaskParams tp);
void avl_insert( avl_node_t *node);
avl_node_t *avl_delete(int x);
void tree_find(CPU_INT32U OSTickCtr);
int avl_node_height( avl_node_t *node );
int avl_balance_factor( avl_node_t *node );
avl_node_t *avl_rotate_leftleft( avl_node_t *node );
avl_node_t *avl_rotate_leftright( avl_node_t *node );
avl_node_t *avl_rotate_rightleft( avl_node_t *node );
avl_node_t *avl_rotate_rightright( avl_node_t *node );
avl_node_t *avl_balance_node( avl_node_t *node );
void avl_balance( avl_tree_t *tree );
avl_node_t *avl_find( avl_tree_t *tree, int releasetime );
void ready_list_insert();
void update_node(avl_tree_t *tree, CPU_INT32U OSTickCtr);

struct taskparams
{
    OS_TCB *p_tcb;
    OS_TCB p_tcb1;
    TASK_PERIOD period;
    RELEASE_TIME ReleaseTime;
       
};

struct avl_node_s
{
    struct avl_node_s *left,*right;
    TaskParams tparam;
    
};

struct avl_tree_s {
	
	struct avl_node_s *root;
	
};

/* Create a new AVL tree. */
avl_tree_t *avl_create() {
        OSMemCreate((OS_MEM*) &MemBlock, "Memory_alloc_init", &Memory_alloc_init[0][0], (OS_MEM_QTY)10, (OS_MEM_SIZE)(10*sizeof(CPU_INT32U)), &err);
	tree = (avl_tree_t*)OSMemGet(&MemBlock,&err);
	tree->root = NULL;

	return tree;
}

/* Initialize a new node. */
avl_node_t *avl_create_node(TaskParams tp) {
	avl_node_t *avlnode = NULL;
	
	avlnode = (avl_node_t*)OSMemGet(&MemBlock,&err);

	avlnode->left = NULL;
	avlnode->right = NULL;
	avlnode->tparam.ReleaseTime = tp.ReleaseTime;
	avlnode->tparam.p_tcb = tp.p_tcb;
        //avlnode->tparam.p_tcb1 = tp.p_tcb1;
	avlnode->tparam.period = tp.period;
        return avlnode;
}

/* Find the height of an AVL node recursively */
int avl_node_height( avl_node_t *node ) {
	int height_left = 0;
	int height_right = 0;

	if( node->left ) height_left = avl_node_height( node->left );
	if( node->right ) height_right = avl_node_height( node->right );

	return height_right > height_left ? ++height_right : ++height_left;
}

/* Find the balance of an AVL node */
int avl_balance_factor( avl_node_t *node ) {
	int bf = 0;

	if( node->left  ) bf += avl_node_height( node->left );
	if( node->right ) bf -= avl_node_height( node->right );

	return bf ;
}

/* Left Left Rotate */
avl_node_t *avl_rotate_leftleft( avl_node_t *node ) {
 	avl_node_t *a = node;
	avl_node_t *b = a->left;
	
	a->left = b->right;
	b->right = a;

	return( b );
}

/* Left Right Rotate */
avl_node_t *avl_rotate_leftright( avl_node_t *node ) {
	avl_node_t *a = node;
	avl_node_t *b = a->left;
	avl_node_t *c = b->right;
	
	a->left = c->right;
	b->right = c->left; 
	c->left = b;
	c->right = a;

	return( c );
}

/* Right Left Rotate */
avl_node_t *avl_rotate_rightleft( avl_node_t *node ) {
	avl_node_t *a = node;
	avl_node_t *b = a->right;
	avl_node_t *c = b->left;
	
	a->right = c->left;
	b->left = c->right; 
	c->right = b;
	c->left = a;

	return( c );
}

/* Right Right Rotate */
avl_node_t *avl_rotate_rightright( avl_node_t *node ) {
	avl_node_t *a = node;
	avl_node_t *b = a->right;
	
	a->right = b->left;
	b->left = a; 

	return( b );
}

/* Balance a given node */
avl_node_t *avl_balance_node( avl_node_t *node ) {
	avl_node_t *newroot = NULL;

	/* Balance our children, if they exist. */
	if( node->left )
		node->left  = avl_balance_node( node->left  );
	if( node->right ) 
		node->right = avl_balance_node( node->right );

	int bf = avl_balance_factor( node );

	if( bf >= 2 ) {
		/* Left Heavy */	

		if( avl_balance_factor( node->left ) <= -1 ) 
			newroot = avl_rotate_leftright( node );
		else 
			newroot = avl_rotate_leftleft( node );

	} else if( bf <= -2 ) {
		/* Right Heavy */

		if( avl_balance_factor( node->right ) >= 1 )
			newroot = avl_rotate_rightleft( node );
		else 
			newroot = avl_rotate_rightright( node );

	} else {
		/* This node is balanced -- no change. */

		newroot = node;
	}

	return( newroot );	
}

/* Balance a given tree */
void avl_balance( avl_tree_t *tree ) {

	avl_node_t *newroot = NULL;

	newroot = avl_balance_node( tree->root );

	if( newroot != tree->root )  {
		tree->root = newroot; 
	}
}

/* Insert a new node. */
void avl_insert(avl_node_t *node) {
	//avl_node_t *node = NULL;
	avl_node_t *next = NULL;
	avl_node_t *last = NULL;

	/* Well, there must be a first case */ 	
	if( tree->root == NULL ) {
		//node = avl_create_node();
		//node->value = value;

		tree->root = node;

	/* Okay.  We have a root already.  Where do we put this? */
	} else {
		next = tree->root;

		while( next != NULL ) {
			last = next;

			if( node->tparam.ReleaseTime < next->tparam.ReleaseTime ) {
				next = next->left;

			} else if( node->tparam.ReleaseTime > next->tparam.ReleaseTime ) {
				next = next->right;

			/* Have we already inserted this node? */
			} else if( node->tparam.ReleaseTime == next->tparam.ReleaseTime ) {
				break;/* This shouldn't happen. */	
			}
		}

		//node = avl_create_node();
		//node->value = value;

		if( node->tparam.ReleaseTime < last->tparam.ReleaseTime ) last->left = node;
		if( node->tparam.ReleaseTime > last->tparam.ReleaseTime ) last->right = node;
		
	}

	avl_balance( tree );
}

/* Find the node containing a given value */
avl_node_t *avl_find( avl_tree_t *tree, int releasetime ) {
	avl_node_t *current = tree->root;

	while( current && current->tparam.ReleaseTime != releasetime ) {
		if( releasetime > current->tparam.ReleaseTime )
			current = current->right;
		else
			current = current->left;
	}

	return current;
}

void ready_list_insert(avl_node_t *readynode){
	OS_PrioInsert(readynode->tparam.p_tcb->Prio);
        OS_RdyListInsertTail(readynode->tparam.p_tcb);                        
	OSSched();
}

void tree_find(CPU_INT32U OSTickCtr){
	update_node(tree, OSTickCtr);
	}
	
void update_node(avl_tree_t *tree, CPU_INT32U OSTickCtr){
	/*avl_node_t *releasenode;
        avl_tree_t *temp;
	releasenode = avl_find(tree, OSTickCtr);
        if(releasenode!=NULL){
	ready_list_insert(releasenode);
        if(releasenode != tree->root)
        {
	releasenode->right = NULL; //More checks to be done
	releasenode->left = NULL;
        }
        else 
        {
          if(tree->root->left != NULL)
          {
            temp->root = tree->root->left;
            temp->root->right = tree->root->right;
          }
          else if(tree->root->right != NULL)
          {
             tree->root = tree->root->right;
             temp->root->right = tree->root->right;
          }
          tree->root = temp->root;
        }
	releasenode->tparam.ReleaseTime = releasenode->tparam.period + OSTickCtr;
	avl_insert(releasenode);
	}*/
        
        avl_node_t *releasenode, *tempnode;
        releasenode = avl_find(tree, OSTickCtr);
        if(releasenode!=NULL){
	ready_list_insert(releasenode);
        tempnode = releasenode;
        tree->root = avl_delete(OSTickCtr);
        tempnode->tparam.ReleaseTime = tempnode->tparam.period + OSTickCtr;
        tempnode->right = NULL;
	tempnode->left = NULL;
        avl_insert(tempnode);
        }
}


avl_node_t * avl_delete(int x)
{
  if(isFirst == 1){
  tree_1 = tree->root;
  isFirst = 0;
  }
    avl_node_t *p;
    
    if(tree_1==NULL)
    {
        return NULL;
    }
    else
        if(x > tree_1->tparam.ReleaseTime)        // insert in right subtree
        {
            tree_1 = tree_1->right;
			tree_1->right=avl_delete(x);
            if(avl_balance_factor(tree_1)==2)
                if(avl_balance_factor(tree_1->left)>=0)
                    tree_1=avl_rotate_leftleft(tree_1);
                else
                    tree_1=avl_rotate_leftright(tree_1);
        }
        else
            if(x<tree_1->tparam.ReleaseTime)
            {   
                tree_1 = tree_1->left;
                tree_1->left=avl_delete(x);
                if(avl_balance_factor(tree_1)==-2)    //Rebalance during windup
                    if(avl_balance_factor(tree_1->right)<=0)
                        tree_1=avl_rotate_rightright(tree_1);
                    else
                        tree_1=avl_rotate_rightleft(tree_1);
            }
            else
            {
                //data to be deleted is found
                tree_1 = tree_1->right;
                if(tree_1->right!=NULL)
                {    //delete its inorder succesor
                    p=tree_1->right;
                    
                    while(p->left!= NULL)
                        p=p->left;
                    
                    tree_1->tparam.ReleaseTime=p->tparam.ReleaseTime;
                    tree_1->right=avl_delete(p->tparam.ReleaseTime);
                    
                    if(avl_balance_factor(tree_1)==2)//Rebalance during windup
                        if(avl_balance_factor(tree_1->left)>=0)
                            tree_1=avl_rotate_leftleft(tree_1);
                        else
                            tree_1=avl_rotate_leftright(tree_1);
                }
                else
                    return(tree_1->left);
            }
    //T->ht=height(T);
	tree->root = tree_1;
    return(tree->root);
}



