#include <os.h>
#include <math.h>
#include <stdio.h>
//#include "splay.h"
#define print_heap 1
extern OS_MEM HeapPartition;
extern CPU_INT08U HeapPartitionStorage[50][100];
OS_TCB *rdylistptr;

heap* Tree=NULL;
OS_ERR  heap_err;
/*********************************Heap*******************************************/
/* RR(Y rotates to the right):
k2                   k1
/  \                 /  \
k1   Z     ==>       X   k2
/ \                      /  \
X   Y                    Y    Z
*/

heap* RR_Rotate_heap(heap* k2)
{
  heap* k1 = k2->lchild;
  k2->lchild = k1->rchild;
  k1->rchild = k2;
  return k1;
}

/* LL(Y rotates to the left):
k2                       k1
/  \                     /  \
X    k1         ==>      k2   Z
/  \                /  \
Y    Z              X    Y
*/
heap* LL_Rotate_heap(heap* k2)
{
  heap* k1 = k2->rchild;
  k2->rchild = k1->lchild;
  k1->lchild = k2;
  return k1;
}

/* An implementation of top-down heap tree
If key is in the tree, then the node containing the key will be rotated to Tree,
 else the last non-NULL node (on the search path) will be rotated to Tree.
*/
heap* Heap(OS_TICK ReleaseTime, heap* Tree)
{
  if(!Tree)
    return Tree;
  //return NULL;
  heap head;
  /* head.rchild points to L tree; head.lchild points to R Tree */
  head.lchild = head.rchild = NULL;
  heap* LeftTreeMax_heap = &head;
  heap* RightTreeMin_heap = &head;
  
  /* loop until Tree->lchild == NULL || Tree->rchild == NULL; then break!
  (or when find the key, break too.)
  The zig/zag mode would only happen when cannot find key and will reach
  null on one side after RR or LL Rotation.
  */
  while(1)
  {
    if(ReleaseTime < Tree->p_tcb_heap->Next_Release)
    {
      if(!Tree->lchild)
        break;
      if(ReleaseTime < Tree->lchild->p_tcb_heap->Next_Release)
      {
        Tree = RR_Rotate_heap(Tree); /* only zig-zig mode need to rotate once,
        because zig-zag mode is handled as zig
        mode, which doesn't require rotate,
        just linking it to R Tree */
        if(!Tree->lchild)
          break;
      }
      /* Link to R Tree */
      RightTreeMin_heap->lchild = Tree;
      RightTreeMin_heap = RightTreeMin_heap->lchild;
      Tree = Tree->lchild;
      RightTreeMin_heap->lchild = NULL;
    }
    else if(ReleaseTime > Tree->p_tcb_heap->Next_Release)
    {
      if(!Tree->rchild)
        break;
      if(ReleaseTime > Tree->rchild->p_tcb_heap->Next_Release)
      {
        Tree = LL_Rotate_heap(Tree);/* only zag-zag mode need to rotate once,
        because zag-zig mode is handled as zag
        mode, which doesn't require rotate,
        just linking it to L Tree */
        if(!Tree->rchild)
          break;
      }
      /* Link to L Tree */
      LeftTreeMax_heap->rchild = Tree;
      LeftTreeMax_heap = LeftTreeMax_heap->rchild;
      Tree = Tree->rchild;
      LeftTreeMax_heap->rchild = NULL;
    }
    else
      break;
  }
  /* assemble L Tree, Middle Tree and R tree together */
  LeftTreeMax_heap->rchild = Tree->lchild;
  RightTreeMin_heap->lchild = Tree->rchild;
  Tree->lchild = head.rchild;
  Tree->rchild = head.lchild;
  
  return Tree;
}

heap* New_Node_heap(OS_TCB *p_tcb_)
{
  
  //heap* p_node = new heap;
  heap *p_node = (heap *)OSMemGet((OS_MEM *)&HeapPartition,(OS_ERR *)&heap_err);
  //heap *p_node = (heap *)malloc(sizeof(heap));
#ifdef print_heap
  printf("\nNew Memory Allocated");
#endif
  p_node->p_tcb_heap = p_tcb_;
  p_node->lchild = p_node->rchild = NULL;
  return p_node;
}

/* Implementation 1:
First Heap(key, Tree)(and assume the tree we get is called *), so Tree node and
its left child tree will contain nodes with keys <= key, so we could rebuild
the tree, using the newly alloced node as a Tree, the children of original tree
*(including Tree node of *) as this new node's children.
NOTE: This implementation is much better! Reasons are as follows in implementation 2.
NOTE: This implementation of heap tree doesn't allow nodes of duplicate keys!
*/
heap* Insert_heap(OS_TCB *p_tcb_, heap* Tree)
{
  //static heap* p_node = NULL;
  heap* p_node = NULL;
  /* if(!p_node){
  p_node = New_Node(p_tcb_);
}
        else // could take advantage of the node remains because of there was duplicate key before.     
  p_node->p_tcb_heap = p_tcb_;*/
  
  p_node = (heap *)OSMemGet((OS_MEM *)&HeapPartition,(OS_ERR *)&heap_err);
  
#ifdef print_heap
  printf("\nNew Memory Allocated");
#endif
  p_node->p_tcb_heap = p_tcb_;
  p_node->lchild = p_node->rchild = NULL;
  
  /***************initialize the stack*************/
  p_node->p_tcb_heap->StkPtr = OSTaskStkInit(p_node->p_tcb_heap->TaskEntryAddr,
                                             p_node->p_tcb_heap->TaskEntryArg,
                                             p_node->p_tcb_heap->StkBasePtr,
                                             p_node->p_tcb_heap->StkLimitPtr,
                                             p_node->p_tcb_heap->StkSize,
                                             p_node->p_tcb_heap->Opt);
  
  
  if(!Tree)
  {
#ifdef print_heap
    printf("\nFirst Tree...");
#endif
    Tree = p_node;
    p_node = NULL;
    return Tree;
  }
  Tree = Heap(p_tcb_->Next_Release, Tree);
  if(p_tcb_->Next_Release == Tree->p_tcb_heap->Next_Release)   //duplicate values
  {
    if(Tree->dup_nbr == 0)
    {
      Tree->next = p_node;
      Tree->dup_nbr++;
      p_node = NULL;
      return Tree;
    }
    else
    {
      heap *temp = Tree;
      (temp->dup_nbr)++;
      while(temp->next != NULL)
      {
        temp = temp->next;
      }
      temp->next = p_node;
      p_node = NULL;
      return Tree;
    }
  }
  /* This is BST that, all keys <= Tree->key is in Tree->lchild, all keys >
  Tree->key is in Tree->rchild. (This BST doesn't allow duplicate keys.) NOW IT DOES...!!! */
  if(p_tcb_->Next_Release < Tree->p_tcb_heap->Next_Release)
  {
    p_node->lchild = Tree->lchild;
    p_node->rchild = Tree;
    Tree->lchild = NULL;
    Tree = p_node;
#ifdef print_heap
    printf("\n Tree left 1...");
#endif
  }
  else if(p_tcb_->Next_Release > Tree->p_tcb_heap->Next_Release)
  {
    p_node->rchild = Tree->rchild;
    p_node->lchild = Tree;
    Tree->rchild = NULL;
    Tree = p_node;
#ifdef print_heap
    printf("\n Tree right 1...");
#endif
  }
  else
    return Tree;
  p_node = NULL;
  return Tree;
}

/****************************************DELETE TASK**********************************************/
heap* Delete_heap(OS_TCB *p_tcb_, heap* Tree)
{
  heap* temp;
  heap* prev_temp;
  if(!Tree)
    return Tree;
  //return NULL;
  Tree = Heap(p_tcb_->Next_Release, Tree);
  if(p_tcb_->Next_Release != Tree->p_tcb_heap->Next_Release) // No such node in heap tree
    return Tree;
  // Node with this Next_Release value found, now have to search for a particular TCB
  else
  {
    // node to be deleted is the only node
    if((Tree->p_tcb_heap == p_tcb_) &&(Tree->dup_nbr == 0))
    {
      if(!Tree->lchild)
      {
        temp = Tree;
        Tree = Tree->rchild;
      }
      else
      {
        temp = Tree;
        /*Note: Since key == Tree->key, so after Heap(key, Tree->lchild),
        the tree we get will have no right child tree. (key > any key in
        oot->lchild)*/
        Tree = Heap(p_tcb_->Next_Release, Tree->lchild);
        Tree->rchild = temp->rchild;
      }
      OSMemPut((OS_MEM *)&HeapPartition,(heap *)temp,(OS_ERR *)&heap_err);
      //free(temp);
      return Tree;
    }
    
    // else if node to be deleted is the first node, but duplicates are present
    else if((Tree->p_tcb_heap == p_tcb_) &&(Tree->dup_nbr > 0))
    {
      temp = Tree->next;
      Tree->dup_nbr = Tree->dup_nbr - 1;
      Tree->next = temp->next;
      Tree->p_tcb_heap = temp->p_tcb_heap;
      OSMemPut((OS_MEM *)&HeapPartition,(heap *)temp,(OS_ERR *)&heap_err);
      return Tree;
    }
    
    //else if node to be deleted in inside linked list, have to traverse till there
    //and then delete
    else if((Tree->dup_nbr > 0) && (Tree->p_tcb_heap != p_tcb_))
    {
      temp = Tree;
      while(temp->p_tcb_heap != p_tcb_)
      {
        prev_temp = temp;
        temp = temp->next;
      }
      Tree->dup_nbr = Tree->dup_nbr - 1;
      prev_temp->next = temp->next;
      OSMemPut((OS_MEM *)&HeapPartition,(heap *)temp,(OS_ERR *)&heap_err);
      return Tree;
    }
  }
}

void ReadyListInsert(void)
{
  heap *MinNode = find_min();
  
  if(MinNode == NULL)
  {
  }
  else if(rdylistptr == NULL)
  {
    if(MinNode != NULL)
    {
      OS_TaskRdy(MinNode->p_tcb_heap);
      rdylistptr = MinNode->p_tcb_heap;
      OSTaskQty++;
      Tree = Delete_heap(MinNode->p_tcb_heap, Tree);
    }
  }
  
  else if(MinNode->p_tcb_heap-> Next_Release < rdylistptr -> Next_Release)
  {    
    CPU_SR_ALLOC();
    OS_CRITICAL_ENTER();
    OS_RdyListRemove(rdylistptr);
    //  OSTaskQty--;           
    OS_CRITICAL_EXIT_NO_SCHED();
    Tree = Insert_heap(rdylistptr, Tree);
    OS_TaskRdy(MinNode->p_tcb_heap); 
    rdylistptr = MinNode->p_tcb_heap;
    //  OSTaskQty++;
    Tree = Delete_heap(MinNode->p_tcb_heap, Tree);
  }
}

// called from OSRecTaskDelete function
void rdylist_delete(void)
{
  heap* MinNode = find_min();
  if(MinNode == NULL)
  {
    rdylistptr = NULL;
  }
  else 
  {
    OS_TaskRdy(MinNode->p_tcb_heap); 
    rdylistptr = MinNode->p_tcb_heap;
    OSTaskQty++;
    Tree = Delete_heap(MinNode->p_tcb_heap, Tree);
  }
}



heap* Search_heap(OS_TICK key, heap* Tree)
{
  return Heap(key, Tree);
}

/*heap* find_min()
{
  heap* current = Tree;
  if(Tree == NULL)
  {
    return NULL;
  }
  else if(current->lchild != NULL)
  {
    Tree = current->lchild;
    find_min();
    return Tree;
  }
  else
  {
    return current;
  }
}*/

heap* find_min()
{
  heap* current = Tree;
  if(current == NULL)
  {
    return NULL;
  }
  else while(current->lchild != NULL)
  {
    current = current->lchild;
  }
  return current;
}